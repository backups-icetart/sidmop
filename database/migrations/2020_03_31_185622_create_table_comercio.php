<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableComercio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comercios', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->string('denominacion')->nullable();
            $table->string('tipo_documento');
            $table->string('nro_documento');
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable();
            $table->string('estado_contribuyente')->nullable();
            $table->string('condicion_domicilio')->nullable();
            $table->string('ubigeo')->nullable();
            $table->string('departamento')->nullable();
            $table->string('provincia')->nullable();
            $table->string('distrito')->nullable();
            $table->string('lat');
            $table->string('lng');
            $table->integer('idusuario')->nullable();   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comercios');
    }
}
