<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'username' => 'admin',
            'nombre' => Str::random(5),
            'apellido' => Str::random(10),
            'direccion' => Str::random(10).' S/N',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'password' => md5('123'),
            'rol' => 'Supervisor',
            'lat' => '-75.545',
            'lng' => '-12.454',
            'idcreator' => 1,
            'email' => 'admin@icetart.com',
            '_new' => false,
        ]);

        DB::table('usuarios')->insert([
            'username' => 'rgarcia',
            'nombre' => Str::random(5),
            'apellido' => Str::random(10),
            'direccion' => Str::random(10).' S/N',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'password' => md5('123'),
            'rol' => 'Ejecutivo',
            'lat' => '-75.545',
            'lng' => '-12.454',
            'idcreator' => 1,
            'email' => 'rgarcia@icetart.com',
            '_new' => false,
        ]);

        DB::table('usuarios')->insert([
            'username' => 'cmamani',
            'nombre' => Str::random(5),
            'apellido' => Str::random(10),
            'direccion' => Str::random(10).' S/N',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'password' => md5('123'),
            'rol' => 'Ejecutivo',
            'lat' => '-75.545',
            'lng' => '-12.454',
            'idcreator' => 1,
            'email' => 'cmamani@icetart.com',
            '_new' => false,
        ]);

        DB::table('usuarios')->insert([
            'username' => 'apriale',
            'nombre' => Str::random(5),
            'apellido' => Str::random(10),
            'direccion' => Str::random(10).' S/N',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'password' => md5('123'),
            'rol' => 'Ejecutivo',
            'lat' => '-75.545',
            'lng' => '-12.454',
            'idcreator' => 1,
            'email' => 'apriale@icetart.com',
            '_new' => false,
        ]);
    }
}
