<?php

use Illuminate\Database\Seeder;

class ComerciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comercios')->insert([
            'codigo' => '001',
            'denominacion' => 'Renzo Garcia',
            'tipo_documento' => 'D.N.I.',
            'nro_documento' => '71799113',
            'direccion' => 'Pasaje Atlanta 170',
            'telefono' => '064254928',
            'estado_contribuyente' => 'CASADO',
            'condicion_domicilio' => 'ALQUILADO',
            'ubigeo' => '050125',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'lat' => '-75.25',
            'lng' => '-12.55',
            'idusuario' => 1,
        ]);

        DB::table('comercios')->insert([
            'codigo' => '002',
            'denominacion' => 'Renzo Garcia',
            'tipo_documento' => 'D.N.I.',
            'nro_documento' => '71799113',
            'direccion' => 'Pasaje Atlanta 170',
            'telefono' => '064254928',
            'estado_contribuyente' => 'CASADO',
            'condicion_domicilio' => 'ALQUILADO',
            'ubigeo' => '050125',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'lat' => '-75.25',
            'lng' => '-12.55',
            'idusuario' => 2,
        ]);

        DB::table('comercios')->insert([
            'codigo' => '003',
            'denominacion' => 'Renzo Garcia',
            'tipo_documento' => 'D.N.I.',
            'nro_documento' => '71799113',
            'direccion' => 'Pasaje Atlanta 170',
            'telefono' => '064254928',
            'estado_contribuyente' => 'CASADO',
            'condicion_domicilio' => 'ALQUILADO',
            'ubigeo' => '050125',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'lat' => '-75.25',
            'lng' => '-12.55',
            'idusuario' => 3,
        ]);

        DB::table('comercios')->insert([
            'codigo' => '004',
            'denominacion' => 'Renzo Garcia',
            'tipo_documento' => 'D.N.I.',
            'nro_documento' => '71799113',
            'direccion' => 'Pasaje Atlanta 170',
            'telefono' => '064254928',
            'estado_contribuyente' => 'CASADO',
            'condicion_domicilio' => 'ALQUILADO',
            'ubigeo' => '050125',
            'departamento' => 'JUNIN',
            'provincia' => 'HUANCAYO',
            'distrito' => 'EL TAMBO',
            'lat' => '-75.25',
            'lng' => '-12.55',
            'idusuario' => 4,
        ]);

    }
}
