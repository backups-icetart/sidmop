<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Polygon;
use DB;

class PolygonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Polygon::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $polygon = new Polygon;
        $polygon->coord = $request->coord;
        $polygon->nombre = $request->nombre;
        $polygon->idcreator = $request->idcreator;
        $polygon->save();
        $lastInsertId = $polygon->id;
        return response()->json(['success'=>'true' , 'message' => 'Coordenada registrada correctamente.', 'id' => $lastInsertId ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $polygon = DB::table('polygons')->where('id', '=', $id)->get();
        return $polygon;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
    public function obtenercoordCreator($id){
        $polygon = DB::table('polygons')->where('idcreator', '=', $id)->get();
        return $polygon;
    }

    public function obtenercoordCreatorSup($id)
    {       
        $polygon = DB::table('polygons')
            ->join('usuarios', 'usuarios.id', '=', 'polygons.idcreator')
            ->select('usuarios.nombre','usuarios.apellido','usuarios.direccion','polygons.*')
            ->where('usuarios.id', '=', $id)
            ->orwhere('usuarios.idcreator', '=', $id)
            ->get();

        return $polygon;
    }

    public function obtenerCantidadCreator($id){
        $count = DB::table('polygons')->where('idcreator', '=', $id)->count();
        return $count;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if ($request->coord == '' || $request->nombre == ''){
            return response()->json(['success'=>'false' , 'message' => 'Datos incorrectos, no se puede actualizar.']);
        }
        else{
            $update = DB::select('update polygons set coord = ?, nombre = ?, idcreator = ? where id = ?', array($request->coord, $request->nombre, $request->idcreator, $request->id));
            return response()->json(['success'=>'true' , 'message' => 'Coordenadas actualizadas correctamente.']);  
        }         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $polygon = Polygon::findOrFail($id);
        $polygon->delete();
        return response()->json(['success'=>'true' , 'message' => 'Coordenadas elimanadas correctamente.']);
    }
}
