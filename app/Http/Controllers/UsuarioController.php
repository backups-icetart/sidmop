<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuarios;
use DB;

class UsuarioController extends Controller
{
    public function abece()
    {
        echo "Hola";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Usuarios::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = DB::table('usuarios')->where('username', '=', $request->username)->count();
        if ($users > 0) {
            return response()->json(['success'=>'false' , 'message' => 'El usuario ya existe.']);
        }
        else {
            $usuarios = new Usuarios;
            //Declaramos el nombre con el nombre enviado en el request
            $usuarios->username = $request->username;
            $usuarios->email = $request->email;
            $usuarios->nombre = $request->nombre;
            $usuarios->apellido = $request->apellido;
            $usuarios->direccion = $request->direccion;
            $usuarios->departamento = $request->departamento;
            $usuarios->provincia = $request->provincia;
            $usuarios->distrito = $request->distrito;
            $usuarios->password = md5($request->password);
            $usuarios->rol = 'Ejecutivo';
            $usuarios->lat = $request->lat;
            $usuarios->lng = $request->lng; 
            $usuarios->idcreator = $request->idcreator; 
            $usuarios->_new = false; 
            //Guardamos el cambio en nuestro modelo
            $usuarios->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //return Pokemon::where('id', $id)->get();
        //return Usuarios::where('id', $id)->get();

        $users = DB::table('usuarios')->where('id', '=', $id)->get();
        return $users;
    }

    public function showEmail($email)
    {
        $users = DB::table('usuarios')->where('email', '=', $email)->where('rol', '=', 'Ejecutivo')->get();
        return $users;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuarios = Usuarios::find($id);
        $usuario = $request->all();
        //return gettype($usuario);
        if (isset($request->password)){
            $usuario['password'] = md5($request->password);
        }
        //return $usuario;
        $usuarios->update($usuario);
        return $usuarios;
    }

    public function login(Request $request)
    {
        
        $request->validate([
            'username'       => 'required|string',
            'password'    => 'required|string'
        ]);

        if (Usuarios::where('username', '=', $request->username)->exists()) {
            if(DB::table('usuarios')->where('username', '=', $request->username)->where('password', '=', md5($request->password))->first()){
                //$login = DB::table('usuarios')->where('username', '=', $request->username)->where('password', '=', md5($request->password))->get();
                //$login = Usuarios::where('username', '=', $request->username)->where('password', '=', $request->password)->get();
                //$update = DB::select('update usuarios set lat = ?, lng = ? where username = ?', array($request->lat, $request->lng, $request->username));
                $login = DB::select('select id,username,nombre,apellido,direccion,departamento,provincia,distrito,rol,lat,lng,_new,idcreator from usuarios where username = ?', array($request->username));               
                $update = DB::select('update usuarios set _new = ? where username = ?', array(true, $request->username));
                return $login;
            }
            else{
                return response()->json(['success'=>'false' , 'message' => 'Contraseña incorrecta.']);
            }
        }
        else{
            return response()->json(['success'=>'false' , 'message' => 'El usuario no existe.']);
        }
        
    }

    public function obtenerUsuario($id){
        $users = DB::table('usuarios')->where('idcreator', '=', $id)->count();
        if ($users > 0) {
            $supervisados = DB::table('usuarios')->where('idcreator', '=', $id)->get();
            return $supervisados;
        }
        else {
            return response()->json(['success'=>'false' , 'message' => 'No tiene a usuarios supervisados.']);
        }
    }

    public function updatecoordenadas(Request $request)
    {       
        if ($request->lat == '' || $request->lng == ''){
            return response()->json(['success'=>'false' , 'message' => 'Coordenadas no validas.']);
        }
        else{
            $update = DB::select('update usuarios set lat = ?, lng = ? where id = ?', array($request->lat, $request->lng, $request->id));
            return response()->json(['success'=>'true' , 'message' => 'Coordenadas actualizadas correctamente.']);  
        }         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = Usuarios::findOrFail($id);
        $usuarios->delete();
        return 'Registro fue eliminado correctamente.';
    }
}
