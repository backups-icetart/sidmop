<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use DB;

class ComercioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Comercio::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comercios = new Comercio;
        //Declaramos el nombre con el nombre enviado en el request
        $comercios->codigo = $request->codigo;
        $comercios->denominacion = $request->denominacion;
        $comercios->tipo_documento = $request->tipo_documento;
        $comercios->nro_documento = $request->nro_documento;
        $comercios->direccion = $request->direccion;
        $comercios->telefono = $request->telefono;
        $comercios->estado_contribuyente = $request->estado_contribuyente;
        $comercios->condicion_domicilio = $request->condicion_domicilio;
        $comercios->ubigeo = $request->ubigeo;
        $comercios->departamento = $request->departamento;
        $comercios->provincia = $request->provincia;
        $comercios->distrito = $request->distrito;
        $comercios->lat = $request->lat;
        $comercios->lng = $request->lng;
        $comercios->idusuario = $request->idusuario; 
        //Guardamos el cambio en nuestro modelo
        $comercios->save();

        return response()->json($comercios, 201);
    }
    
    public function obtenercomercios($id)
    {       
        $comercios = DB::table('comercios')
            ->join('usuarios', 'usuarios.id', '=', 'comercios.idusuario')
            ->select('usuarios.nombre','usuarios.apellido','usuarios.direccion','comercios.*')
            ->where('usuarios.id', '=', $id)
            ->orwhere('usuarios.idcreator', '=', $id)
            ->get();

        return $comercios;
    }
    public function obtenerCantidadCreator($id){
        $count = DB::table('comercios')->where('idusuario', '=', $id)->count();
        return $count;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Comercio::where('idusuario', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comercios = Comercio::find($id);
        $comercios->update($request->all());
        return $comercios;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comercios = Comercio::findOrFail($id);
        $comercios->delete();
        return response()->json(['success'=>'true' , 'message' => 'Comercios eliminado correctamente.']);  
    }
}
