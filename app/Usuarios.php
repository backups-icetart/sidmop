<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Usuarios extends Model
{
    protected $fillable = ['username', 'nombre', 'apellido', 'direccion', 'departamento', 'provincia', 'distrito', 'password', 'rol', 'lat', 'lng', '_new', 'idcreator','email'];

    protected $hidden = [
        'password',
    ];
}

/*
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Usuarios as Authenticatable;

class Usuarios extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $fillable = [
        'username', 'nombre', 'apellido', 'direccion', 'departamento', 'provincia', 'distrito', 'rol', 'lat', 'lng'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}*/