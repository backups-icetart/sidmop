<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mercado extends Model
{
    protected $fillable = ['id','nombre_mercado','ao_inicio','ccdd','departamento','ccpp','provincia','ccdi','distrito','no_dispone','gps_alt','tipo_via','nom_via','nro_puerta','block','interior','piso','mz','lote','km','referencia','n_puestos_fijos','n_puestos_en_funcionamiento','n_de_socios_titulares_o_dueos','p36a_1','p36a_2','p36b_1','p36b_2','p37','p38','puestos_fijos_en_funcionamiento_hay_en_verduras','puestos_fijos_en_funcionamiento_hay_en_frutas','puestos_fijos_en_funcionamiento_hay_en_carnes','puestos_fijos_en_funcionamiento_hay_en_aves','puestos_fijos_en_funcionamiento_hay_en_pescados_y_mariscos','puestos_fijos_en_funcionamiento_hay_en_abarrotes','puestos_fijos_en_funcionamiento_hay_en_expendio_de_comidas','puestos_fijos_en_funcionamiento_hay_en_articulos_de_limpieza','puestos_fijos_en_funcionamiento_hay_en_otro','p39_9_otro','total_de_puestos_fijos','p40_1','p40_1_tiempo','p40_2','p40_2_tiempo','locales_comerciales_alrededor_del_mercado','el_mercado_es_propiedad_de','x','y'];
}
