<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercio extends Model
{
    protected $fillable = ['codigo', 'denominacion', 'tipo_documento', 'nro_documento', 'direccion', 'telefono', 'estado_contribuyente', 'condicion_domicilio', 'ubigeo', 
    'departamento', 'provincia', 'distrito', 'lat', 'lng', 'idusuario'];
}
