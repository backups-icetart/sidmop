<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
*/
Route::post('/login', 'UsuarioController@login');
Route::post('/updatecoord', 'UsuarioController@updatecoordenadas');
Route::get('/getuser/{id}', 'UsuarioController@obtenerUsuario');
Route::get('/getejecutivo/{email}', 'UsuarioController@showEmail');

Route::get('/getcantcoor/{id}', 'PolygonController@obtenerCantidadCreator');
Route::get('/getcoordbycreator/{id}', 'PolygonController@obtenercoordCreator');
Route::get('/getcoordbycreatorsup/{id}', 'PolygonController@obtenercoordCreatorSup');


Route::get('/obtenercomercios/{id}', 'ComercioController@obtenercomercios');
Route::get('/getcantcomercio/{id}', 'ComercioController@obtenerCantidadCreator');

Route::get('/obtenermercados', 'MercadoController@obtenermercados');
Route::get('/obtenermercadoscoor', 'MercadoController@obtenercoordenadas');

Route::resource('usuarios', 'UsuarioController');
Route::resource('polygon', 'PolygonController');
Route::resource('comercios', 'ComercioController');
Route::resource('mercados', 'MercadoController');